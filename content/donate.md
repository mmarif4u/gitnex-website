+++
title = "Donate"
description = "Every dollar counts and that's where all of you good people show your love for GitNex."

+++

## Become a Patreon
[<img alt="Become a Patroen" src="/img/become_a_patron_button.png" id="patreon_button"/>](https://www.patreon.com/mmarif)
